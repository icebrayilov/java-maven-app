FROM maven:3-jdk-8

WORKDIR /app

COPY pom.xml .

RUN mvn dependency:go-offline

COPY src ./src

RUN mvn package

CMD ["java", "-jar", "target/app-1.0.jar"]